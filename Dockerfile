FROM clojure
COPY . /usr/src/app
WORKDIR /usr/src/app
CMD ["lein", "ring", "server-headless", "5000"]
EXPOSE 5000
ENV POSTGRES_DB sample
ADD . $POSTGRES_DB
ENV DATABASE_URL "postgresql://postgres@10.138.0.3:5432/scalors"
ADD . $DATABASE_URL
